#!/bin/sh

# Clean up
rm -rf ./docker/node-release/app

# Build
(cd ./app/demo-server/ && npm run build)
(cd ./app/demo-client/ && npm run build)

# Move builded files
mv ./app/demo-server/dist ./docker/node-release/app
cp ./app/demo-server/package.json ./docker/node-release/app
mv ./app/demo-client/bundle ./docker/node-release/app/bundle
cp ./app/demo-client/package.json ./docker/node-release/app/bundle

# Add init scripts
cp ./docker/node-release/bin/ecosystem.config.js ./docker/node-release/app/
cp ./docker/node-release/bin/start.sh ./docker/node-release/app/

# Build docker Image
docker build -t registry.gitlab.com/walker-walks/fullstack-demo-app ./docker/node-release

