import { dbConectionFactory } from '../infrastructure/server/dbconn.factory';
import { DBCONFIG } from '../config/dbconfig'
import { User } from '../domain/user.model';
import { Book } from '../domain/book.model';
import { UserDatas } from './datas/user-datas';
import { BookDatas } from './datas/book-datas';
import * as path from 'path';
import * as fs from 'fs';


const sequelize = dbConectionFactory(DBCONFIG);
const init = async () => {
  const dbPath = path.resolve(process.cwd(), DBCONFIG.storage);
  console.log('path', dbPath, fs.existsSync(dbPath));
  if (fs.existsSync(dbPath)) {
    console.log('DB is already installed.');
    return;
  }
  await sequelize.sync();
  await Promise.all([
    User.bulkCreate(UserDatas),
    Book.bulkCreate(BookDatas),
  ]);
  console.log('Installed successfuly.');
}

init().catch(console.log)