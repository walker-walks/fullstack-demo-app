import { Module } from '@nestjs/common'

import { BookController } from './book.controller'
import { BookRepository } from '../infrastructure/repository/book.repository';

@Module({
  controllers: [ BookController ],
  components: [ BookRepository ],
})
export class BookModule {}