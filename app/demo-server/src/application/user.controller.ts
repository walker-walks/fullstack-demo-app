import {
  Controller,
  Get, Req,
  Res, HttpStatus,
  Post, Body, Param
} from "@nestjs/common"
import { UserRepository } from '../infrastructure/repository/user.repository';


@Controller('user')
export class UserController {

  constructor(private readonly userRepository: UserRepository) {

  }

  @Get('/by-name/:name')
  findOne(@Param('name') name: string) {
    return this.userRepository.findByName(name);
  }

}
