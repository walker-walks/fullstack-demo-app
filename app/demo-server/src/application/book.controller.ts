import {
  Controller,
  Get, Req,
  Res, HttpStatus,
  Post, Body, Param
} from "@nestjs/common"
import { BookRepository } from '../infrastructure/repository/book.repository';

@Controller('book')
export class BookController {

  constructor(private readonly bookRepository: BookRepository ) {

  }
  @Get()
  findAll() {
    return 'hello'
  }

  @Get('/by-title/:title')
  findByTitle(@Param('title') title: string) {
    return this.bookRepository.findByTitle(title);
  }

  @Get('/by-author/:author')
  findByAuthor(@Param('author') author: string) {
    return this.bookRepository.findByAuthor(author);
  }

}