import { Module } from '@nestjs/common'

import { UserController } from './user.controller'
import { UserRepository } from '../infrastructure/repository/user.repository';

@Module({
  controllers: [ UserController ],
  components: [ UserRepository ],
})
export class UserModule {}