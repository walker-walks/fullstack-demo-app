import { Sequelize, ISequelizeConfig } from "sequelize-typescript";
import { User } from "../../domain/user.model";
import { Book } from "../../domain/book.model";

export function dbConectionFactory(dbConfig: ISequelizeConfig) {

  const sequelize = new Sequelize(dbConfig);
  sequelize.addModels([User, Book]);
  
  return sequelize;
}