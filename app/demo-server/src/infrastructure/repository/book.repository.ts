import { Component } from "@nestjs/common"
import * as Bluebird from "bluebird";
import { Op } from "sequelize";
import { Book } from "../../domain/book.model";

@Component()
export class BookRepository {

  findByTitle(title: string): Bluebird<Book[]> {
    return Book.findAll({
      where: {
        title: {
          [Op.like]: `%${title}%`,
        },
      }
    });
  }

  findByAuthor(author: string): Bluebird<Book[]> {
    return Book.findAll({
      where: {
        author: {
          [Op.like]: `%${author}%`,
        },
      }
    });
  }

}