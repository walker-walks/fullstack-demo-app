import { Component } from "@nestjs/common"
import { User } from "../../domain/user.model";
import * as Bluebird from "bluebird";
import { Op } from "sequelize";

@Component()
export class UserRepository {

  findByName(name: string): Bluebird<User[]> {
    return User.findAll({
      where: {
        fullName: {
          [Op.like]: `%${name}%`,
        },
      }
    });
  }

}