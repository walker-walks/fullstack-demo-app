import {
  Module, Global,
  NestModule,
  MiddlewaresConsumer,
  RequestMethod
} from '@nestjs/common'

import { AppController } from './app.controller'

import { LoggerMiddleware } from './infrastructure/middleware/logger.middleware';
import { CorsMiddlweare } from './infrastructure/middleware/cors.middleware';
import { UserModule } from './application/user.module';
import { BookModule } from './application/book.module';


@Global()
@Module({
  imports: [
    UserModule,
    BookModule,
  ],
})
export class ApplicationModule implements NestModule {
  configure(consumer: MiddlewaresConsumer): void {
    consumer.apply([ LoggerMiddleware, CorsMiddlweare ])
      .forRoutes({ path: '/**', method: RequestMethod.ALL })
  }
}
