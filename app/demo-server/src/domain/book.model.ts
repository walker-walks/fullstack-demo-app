import { AutoIncrement, Column, DataType, BelongsTo, Model, PrimaryKey, Table, Unique }
from 'sequelize-typescript';

@Table({
  timestamps: false,
  indexes: [{ unique: false, fields: ['title', 'author'] }],
})
export class Book extends Model<Book>{

  @PrimaryKey
  @Column(DataType.INTEGER)
  id: number;

  @Column(DataType.STRING(63))
  title: string;

  @Column(DataType.STRING(63))
  author: string;

  @Column(DataType.INTEGER)
  yearPublished: number;

  @Column(DataType.STRING(63))
  price: string;

  @Column(DataType.STRING(63))
  rating: string;
  
}