import { AutoIncrement, Column, DataType, BelongsTo, Model, PrimaryKey, Table, Unique }
from 'sequelize-typescript';

@Table({
  timestamps: false,
  indexes: [{ unique: false, fields: ['fullName'] }],
})
export class User extends Model<User> {

  @PrimaryKey
  @Column(DataType.INTEGER)
  id: number;

  @Column(DataType.STRING(63))
  fullName: string;

  @Column(DataType.STRING(63))
  gender: string;

  @Column(DataType.INTEGER)
  age: number;

  @Column(DataType.STRING(63))
  email: string;

  @Column(DataType.STRING(63))
  phone: string;

  @Column(DataType.STRING(63))
  username: string;

}