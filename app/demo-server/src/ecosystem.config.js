'use strict';

let defaultConfig = {
  apps : [{
    name        : 'server',
    script      : 'main.js',
    error_file  : 'volume/logs/service-debug.error.log',
    out_file    : 'volume/logs/service-debug.out.log',
    pid_file    : 'volume/logs/service-debug.pid',
    log_date_format: '',
    ignore_watch: ['[\\/\\\\]\\./', 'node_modules', 'public', '.git', '.tscache', 'logs']
  }]
};

if ( process.platform === 'win32' ) {
  defaultConfig.apps[0].watch_options = { usePolling: true };
}
if ( process.env.NODE_ENV != 'production') {
  defaultConfig.apps[0].source_map_support = true;
  defaultConfig.apps[0].node_args =  ['--inspect=5858'];
}


module.exports = defaultConfig;
