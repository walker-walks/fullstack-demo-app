import { ISequelizeConfig } from 'sequelize-typescript';
import { IServerConfig } from '../infrastructure/server/server.config';


export const SERVERCONFIG: IServerConfig = {
  port: process.env.PORT || '3000',
  env: process.env.NODE_ENV || 'development',
  isProdMode: process.env.NODE_ENV === 'production',
}

export const DBCONFIG: ISequelizeConfig = {
  username: '',
  password: '',
  database: process.env.DB_NAME || 'appdb',
  dialect:  'sqlite',
  logging: SERVERCONFIG.isProdMode,
  storage: './volume/dats.sqlite3',
};
